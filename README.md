# gtk-macros
Few macros to make gtk-rs development more convenient

- [Open documentation](https://docs.rs/gtk-macros)
- [gtk-macros on crates.io](https://crates.io/crates/gtk_macros)
